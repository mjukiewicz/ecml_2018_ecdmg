# Source code for ECML_2018 Evaluating Cross-Device Marketing Graphs paper #

Every script is self-contained and meant to be run in spark-shell with Spark version >= 2.1.0.

For the 100% reproducibility we recommend running these script in standalone (local) mode.

The \*-real-data scripts require some external data to be loaded in order to perform evaluation.

The other scripts have no externall dependences whatsoever.
