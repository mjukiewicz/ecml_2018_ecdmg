/* To be run in spark-shell */
import spark.implicits._
import org.apache.spark.sql.functions.{collect_list, avg, sum, lit}
import org.apache.spark.sql.{Dataset, DataFrame}
import org.apache.spark.rdd.RDD
import scala.reflect.ClassTag


def kFoldStratified[K: ClassTag, V: ClassTag](
  rdd: RDD[(K, V)],
  numFolds: Int,
  seed: Long
): Seq[(RDD[(K, V)], RDD[(K, V)])] = {
  val basicSampleProb = 1.0 / numFolds
  val iterativeSampleProbs = (0 until numFolds).
    map(k => basicSampleProb / (1.0 - basicSampleProb * k))

  val indexedRdd = rdd.zipWithUniqueId.map { case ((k, v), i) => (k, (v, i)) }
  indexedRdd.checkpoint()

  iterativeSampleProbs.foldLeft(
    (sc.broadcast(Set.empty[Long]), Seq.empty[(RDD[(K, V)], RDD[(K, V)])])
  ) {
    case ((usedIds, rdds), sampleFraction) =>

      val filteredSet = indexedRdd.
        filter { case (_, (_, i)) => !usedIds.value.contains(i) }.sortBy(_._2._2)

      val fracMap = filteredSet.keys.collect.distinct.map((_, sampleFraction)).toMap

      val testSet = filteredSet.sampleByKeyExact(false, fracMap, seed)

      val usedIdsnew = testSet.map(_._2._2).collect.toSet ++ usedIds.value

      (sc.broadcast(usedIdsnew),
        rdds :+ (testSet.map { case (k, (v, _)) => (k, v) },
          indexedRdd.subtract(testSet).map { case (k, (v, _)) => (k, v) }))
  }._2
}


def constrainSetsToUsersFromCommonDevices(detSet: DataFrame, predSet: DataFrame) = {
  val commonUsers = predSet.toDF("deviceId", "predUserId").
    join(detSet.toDF("deviceId", "detUserId"), "deviceId").
    select($"predUserId", $"detUserId")

  val predUsersConstrained = predSet.
    join(commonUsers.select($"predUserId").distinct(), $"userId" === $"predUserId").
    select($"deviceId", $"userId")

  val detSetConstrained = detSet.
    join(commonUsers.select($"detUserId").distinct(), $"userId" === $"detUserId").
    select($"deviceId", $"userId")

  (detSetConstrained, predUsersConstrained)
}

def prepareCumulativeTestSets(detSet: DataFrame, seed: Int = 1, folds: Int = 10) = {

  def users(
    devices: DataFrame
  ): Dataset[(Int, Seq[Long])] =
    devices.
      groupBy("userId").
      agg(collect_list("deviceId").as("devices")).
      orderBy($"userId").
      select("devices").
      as[Seq[Long]].
      map(d => (d.length, d.sorted)).
      orderBy($"_1", $"_2")

  val testSets = kFoldStratified(users(detSet).rdd, folds, seed).
    map { case (testSet, _) =>
      val testSetDeviceIds = testSet.
        flatMap(_._2).toDF("value")
      val testPairs = testSetDeviceIds.
        join(detSet, testSetDeviceIds.col("value") === detSet.col("deviceId")).
        select("deviceId", "userId")
      (testPairs.count, testPairs.select("userId").distinct.count, testPairs)
    }

  testSets.tail.scanLeft(testSets.head.copy(_3 = testSets.head._3.repartition(spark.sparkContext.defaultParallelism).cache().checkpoint())) {
    case ((devsNum1, usersNum1, ts1), (devsNum2, usersNum2, ts2)) =>
      (devsNum1 + devsNum2, usersNum1 + usersNum2,
        (ts1 union ts2).repartition(spark.sparkContext.defaultParallelism).cache().checkpoint())
  }
}

trait EvaluationMeasure {
  def compute(
    groundTruthPairs: DataFrame,
    mgPairs: DataFrame
  ): Array[Double]

  def getNames: Array[String]
}

def f1Score(precision: Double, recall: Double) =
  2f * precision * recall / (precision + recall)

case class ConnectionBasedMeasure() extends EvaluationMeasure {
  override def compute(
    groundTruthPairs: DataFrame,
    mgPairs: DataFrame
  ): Array[Double] = {
    val gtConnections = getCanonicalConnections(groundTruthPairs).cache
    val mgConnections = getCanonicalConnections(mgPairs).cache

    val commonConnectionsNum = gtConnections.intersect(mgConnections).count.toDouble

    val precision = commonConnectionsNum / mgConnections.count
    val recall = commonConnectionsNum / gtConnections.count

    gtConnections.unpersist()
    mgConnections.unpersist()
    Array(precision, recall, f1Score(precision, recall))
  }

  private def canonicalConnections[T: Ordering](cluster: Seq[T]): Iterator[(T, T)] = {
    import scala.Ordered.orderingToOrdered
    for {
      d1 <- cluster.iterator
      d2 <- cluster.iterator if d1 < d2
    } yield (d1, d2)
  }

  private def getCanonicalConnections(deviceUserPair: DataFrame) =
    deviceUserPair.
      as[(Long, Long)].
      groupByKey(_._2).
      flatMapGroups { case (_, group) =>
      canonicalConnections(group.map(_._1).toSeq)
    }.distinct

  override def getNames: Array[String] =
    Array("conn_based_precision", "conn_based_recall", "conn_based_f1")
}

def testMG(
  mgPairs: DataFrame,
  cumulativeTestSets: Seq[(Long, Long, DataFrame)],
  measures: Seq[EvaluationMeasure]
) = {

  val evals = (for {
    (devsNum, usersNum, gtPairs) <- cumulativeTestSets
  } yield {
    val (limDetParis, limMGPairs) = constrainSetsToUsersFromCommonDevices(gtPairs, mgPairs)

    Seq(devsNum.toDouble, usersNum.toDouble) ++ measures.flatMap(_.compute(limDetParis, limMGPairs))
  }).zipWithIndex

  val evalsNames = Seq("devices_num", "users_num") ++ measures.flatMap(_.getNames)

  evals.
    toDF.
    select(evalsNames.indices.map(i => $"_1".getItem(i)) :+ $"_2": _*).
    toDF(evalsNames :+ "fold": _*)
}

spark.sparkContext.setCheckpointDir("~/tmp/")

def generateRandomGraph(n_u: Int, prob: Double, seed: Int) = {
  val r = new scala.util.Random(seed)
  val users = Iterator.iterate(1L) { i => if (r.nextFloat < prob) i + 1 else i }.takeWhile(_ <= n_u).toList
  val devices = (1L to users.size).toSeq
  (devices zip users).toDF("deviceId", "userId").select($"deviceId".cast("string").cast("long"), $"userId".cast("string").cast("long"))
}

val n = 10000
val p = 0.5
val iters = 10

val randGen = new scala.util.Random(239472)
val seeds = (1 to iters).map(_ => randGen.nextInt()).toList.zipWithIndex
val seedPairs = (1 to iters).map(_ => (randGen.nextInt(), randGen.nextInt())).toList.zipWithIndex

val results = for {
  ((s1, s2), i) <- seedPairs
  (s, j) <- seeds
} yield {
  println(s)
  println(s1 + " " + s2)
  println(s"Iteration $i - $j")

  val GT = generateRandomGraph(n, p, s1).cache
  val MG = generateRandomGraph(n, p, s2).cache

  val cumulativeTestSets = prepareCumulativeTestSets(GT, s)

  val tmpRes = testMG(MG, cumulativeTestSets, Seq(ConnectionBasedMeasure()))
  tmpRes.withColumn("seedGT", lit(s1)).
    withColumn("seedMG", lit(s2)).
    withColumn("seed", lit(s)).
    withColumn("graphsIter", lit(i))
}

val resDiffSeeds = results.tail.foldLeft(results.head)((dfF, df) => dfF.union(df))

resDiffSeeds.write.parquet("conn-based-new-sampling-10-iters.parquet")
resDiffSeeds.count

resDiffSeeds.groupBy("fold").
  agg(avg("conn_based_precision"), stddev_samp("conn_based_precision"), avg("conn_based_recall"), stddev_samp("conn_based_recall"), avg("conn_based_f1"), stddev_samp("conn_based_f1")).
  orderBy("fold").
  show(false)